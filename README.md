# *Introduction*

## _My Profile_
<img src="https://gitlab.com/irfanfairuz/introduction/-/raw/main/passport%20photo/Passport_Photo.jpg" width=150 align=middle>

##
+ _Name:_ Muhammad Arif Irfan Bin Muhammad Fairuz
+ _Age:_ 22 Years old
+ _Course:_ Bachelor of Aerospace Engineering
+ _Staying at:_ Nilai, Negeri Sembilan
#
|strength|weakness|
|--------|--------|
|Flexible  |Self Critism|
|Honest|Too detail oriented|
|Creative|Introverted|
